<?php

/**
 * array config database
 */
return [
    'host'     => '127.0.0.1',
    'username' => 'root',
    'password' => '',
    'database' => 'game',
    'charset'  => 'UTF-8'
];
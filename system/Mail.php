<?php

namespace system;
use system\database\Database;

/**
* 
*/
class Mail
{
	private $to;
	private $subject;
	private $message;
	private $headers;
	
	function __construct($name, $email, $message)
	{
		$this->to = $this->getEmail();
		$this->subject = "Повідомлення від" . htmlspecialchars($name);
		$this->message = htmlspecialchars($message);
		$this->headers = 'From: '. htmlspecialchars($email) . "\r\n" .
    					 'Reply-To: '. htmlspecialchars($email) . "\r\n" .
    					 'X-Mailer: PHP/' . phpversion();

	}

	private function getEmail()
	{
		$db = Database::getInstance();
		$sql = "SELECT email FROM admin";
		$result = $db->query($sql);
		if ($result) 
            {
            
                $row = $result->fetch_assoc();
                return $row['email']; 
            }

        return false;    

	}

	public function send()
	{
		$result = mail($this->to, $this->subject, $this->message, $this->headers);
	}
}
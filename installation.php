<?php

require_once 'system/autoloader.php';
use system\Config;

if (php_sapi_name() === 'cli') {

   $install = new install();

   $install->createDatabase();

   exit();
}


/**
 * This class establishes database
 */
class install
{
	/**
	 * [$mysqli connect to database]
	 * @var [object]
	 */
	private $mysqli;

	/**
	 * [$db config database]
	 * @var [array]
	 */
	private $db;

	/**
	 * [$tables database tables]
	 * @var [array]
	 */
	private $tables;

	/**
	 * [__construct get config to database and connect]
	 */
	public function __construct()
	{

		$this->db = config::getConfig('database');

   		$this->mysqli = new \mysqli ($this->db['host'], $this->db['username'], $this->db['password']);

   		$this->tables = Config::getConfig('databaseTable');	

	}

	/**
	 * [createDatabase this function create database]
	 */
	public function createDatabase()
	{
		
   		if ($this->mysqli->connect_errno !== 0) {

   			echo $this->mysqli->connect_errno . PHP_EOL;

   		} else {

   			echo 'Connection to MySQL established' . PHP_EOL;

   			if ($this->mysqli->select_db($this->db['database']) === false) {

   		 		if ($this->mysqli->query('CREATE DATABASE'.' '.$this->db['database'] . ' CHARACTER SET utf8 COLLATE utf8_general_ci') === true) {

                	echo 'Database created' . PHP_EOL;

                	$this->mysqli->select_db($this->db['database']);

                	$this->createTable();

                	$this->adminRegitration();

         		} else {

                	echo 'Database can not be created';
            	}

   			} else {

           		echo 'Database already created' . PHP_EOL;
       		}

  		}

	}
	

	/**
	 * [createTable this function create table with database]
	 */
	public function createTable()
	{
	
		foreach ($this->tables as $table => $values) {
	
			$sql = 'create table ' . $table  . ' (';

			foreach ($values as $fild => $value) {
				
					$filds[]= $fild . ' ' . $value;

			}

			$sql .= implode(', ', $filds) . ') DEFAULT CHARACTER SET utf8 ENGINE=InnoDB;';

			unset($filds);

			if ($this->mysqli->query($sql) === true) {

        		echo 'Table ' . $table . ' created ' . PHP_EOL;

    		} else {

        		echo "Error created table " . $table . PHP_EOL . $this->mysqli->error . PHP_EOL;
    		}
		}

	}

	public function adminRegitration()
	{

		echo 'Enter admin login' . PHP_EOL;

	   	$login = $_POST['login'];//trim(fgets(STDIN)); // читает одну строку из STDIN

	   	echo 'Enter password' . PHP_EOL;

	   	$password = $_POST['password'];//trim(fgets(STDIN));

	   	echo 'Enter repassword' . PHP_EOL;

	   	$repassword = $_POST['repassword'];//trim(fgets(STDIN));

	   	echo 'Enter email' . PHP_EOL;

	   	$email = $_POST['email'];//trim(fgets(STDIN));

	   	if ($password === $repassword) {

	   		$password = password_hash($password, PASSWORD_DEFAULT);
	   		
	   		$sql = "INSERT INTO admin (login, password, email) VALUES ('$login', '$password', '$email')";

	   		$this->mysqli->query($sql);

	   		echo "OK" . PHP_EOL;
	   	}else {

	   		echo "Passwords do not match" . PHP_EOL;
	   		$this->adminRegitration();
	   	}

	}

}
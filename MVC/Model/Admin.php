<?php

namespace MVC\Model;

use system\session\Session;

use system\database\Database;

use system\Controller;

use system\File;

/**
* 
*/
class Admin 
{

	/**
     * $instance object
     * @var null
     */
    private static $instance = null;

    private $session;

    private $db;

	/**
     * getInstance create or return object
     * @return object | this object
     */
    public static function getInstance()
    {
        if (self::$instance === null) {

            self::$instance = new self();

            self::$instance->session = Session::getInstance();

            self::$instance->db = Database::getInstance();
        }
        
        return self::$instance;
    }


    public function logIn($login, $password)
    {
        $logged = false;

        if (strlen($login) < 12 && strlen($password) < 12)  
        {

            $sql = "SELECT * FROM admin WHERE login='$login'";

            $result = $this->db->query($sql);

            if ($result) {
            
                $row = $result->fetch_assoc();
           
                if (password_verify($password, $row['password'])) 
                {
                
                    $this->session->setSession('logged');

                    $logged =  true;

                } 
            }
        }

        return $logged;
    	
    }

    public function editPassword($password, $newPassword, $rePassword)
    {
        $sql = "SELECT password FROM admin";
        $result = $this->db->query($sql);

            if ($result) 
            {
            
                $row = $result->fetch_assoc();
           
                if (password_verify($password, $row['password'])) 
                {
                    if ($newPassword === $rePassword) 
                    {
                        $newPassword = password_hash($newPassword, PASSWORD_DEFAULT);
                        $sql = "UPDATE admin SET password = '$newPassword'";
                        $result = $this->db->query($sql);
                        if($result)
                        {
                            echo "<script>alert(\"Пароль змінено\");</script>";
                        }  
                        else
                        {
                            echo "<script>alert(\"Виникла помилка\");</script>";
                        }  

                    }
                    else
                    {
                        echo "<script>alert(\"Паролі не співпадають\");</script>";
                    }
                    

                }
                else
                {
                    echo "<script>alert(\"Пароль введено невірно\");</script>";
                }    
            }

    }

    public function editEmail($password, $email)
    {
        $sql = "SELECT password FROM admin";
        $result = $this->db->query($sql);

            if ($result) 
            {
            
                $row = $result->fetch_assoc();
           
                if (password_verify($password, $row['password'])) 
                {
                    $sql = "UPDATE admin SET email = '$email'";
                    $result = $this->db->query($sql);
                    if($result)
                    {
                        echo "<script>alert(\"Email змінено\");</script>";
                    }  
                    else
                    {
                        echo "<script>alert(\"Виникла помилка\");</script>";
                    }  

                }
                else
                {
                    echo "<script>alert(\"Пароль введено невірно\");</script>";
                }    
            }

    }


    public function logStatus()
    {
        $logStatus = false;
        $status = $this->session->getSession();

        if ($status  == 'logged') 
        {

           $logStatus = true;
        }

           return $logStatus;

    }


    public function logOut()
    {
        $status = $this->session->getSession();

        if ($status  == 'logged') 
        {

            $this->session->clear();

        }

    }

    public function addSlider($imgd, $values = [])
    {
        $file = new File($imgd);
        $file->dir = 'img/slider';
        $resultImg = $file->uploadImg();
        if ($resultImg == true)
        {
            $values['image_url'] = 'img/slider/' . $file->getName();
            $resultInsert = $this->db->insert('slider', $values);

            if ($resultInsert == true)
            {
                echo "<script>alert(\"Слайдер додано\");</script>";
            }
            else
            {
                echo "<script>alert(\"Виникла помилка\");</script>";
            }    
        }
        else
        {
            echo "<script>alert(\"Виникла помилка при завантаженні зображення\");</script>";
        }    

    }

    public function addCategory($values = [])
    {

        $result = $this->db->insert('category', $values);

        if ($result == true)
        {
            echo "<script>alert(\"Категорію додано\");</script>";
        }
        else
        {
            echo "<script>alert(\"Виникла помилка\");</script>";
        }    
    }

    public function editCategory($values = [], $where = [])
    {
        $result = $this->db->update('category', $values, $where);

        if ($result == true)
        {
            echo "<script>alert(\"Категорію зредаговано\");</script>";
        } 
        else
        {
            echo "<script>alert(\"Виникла помилка\");</script>";
        }    
    }

    public function deleteCategory($values = [])
    {
        $result = $this->db->delete('category', $values);

        if ($result == true)
        {
            echo "<script>alert(\"Категорію видалено\");</script>";
        } 
        else
        {
            echo "<script>alert(\"Виникла помилка\");</script>";
        }   

    }

    public function deleteSlider($values = [], $img)
    {
          $result = $this->db->delete('slider', $values);

          $delImg = unlink("$img");

        if ($result == true && $delImg == true)
        {
            echo "<script>alert(\"Слайдер видалено\");</script>";
        } 
        else
        {
            echo "<script>alert(\"Виникла помилка\");</script>";
        }     
    } 

    public function editSlider($id)
    {
        
        if ($_FILES['slider']['error'] == 0)
        {
            $img = $_POST['image_url'];
            $delImg = unlink("$img");
            
            if ($delImg == true) {
                $file = new File($_FILES['slider']);
                $file->dir = 'img/slider';
                $resultImg = $file->uploadImg();
            }
            
            if ($resultImg == true && $delImg == true)
            {
                $_POST['image_url'] = 'img/slider/' . $file->getName();
                $result = $this->db->update('slider', $_POST, $id);

                if ($result == true)
                {
                    echo "<script>alert(\"Слайдер зредаговано\");</script>";
                }
                else
                {
                echo "<script>alert(\"Виникла помилка\");</script>";
                }    
            }
            else
            {
                echo "<script>alert(\"Виникла помилка при завантаженні зображення\");</script>";
            }    

        }
        else
        {
            $result = $this->db->update('slider', $_POST, $id);
             if ($result == true)
            {
                echo "<script>alert(\"Слайдер зрадаговано\");</script>";
            }
            else
            {
                echo "<script>alert(\"Виникла помилка\");</script>";
            }    
        }
        
    }

    public function addGame($img, $gallery = [], $values = [])
    {
        //завантаження зображення гри
        $file = new File($img);
        $file->dir = 'img/game';
        $resultImg = $file->uploadImg();

        if ($resultImg == true)
        {   
            //масив для додавання в проміжну таблицю
            $idCategory = $values['id_category'];
            unset($values['id_category']);
           
            //додавання гри у БД
            $values['image_url'] = 'img/game/' . $file->getName();
            $resultInsert = $this->db->insert('game', $values);

            //отримуємо id гри
            $title = $values['title'];
            $sql = "SELECT * FROM game WHERE title ='$title'";
            $result = $this->db->getRow($sql);
            $category['id_game'] = $result[0]->id;

            //записуємо дані у проміжну таблицю
            foreach ($idCategory as $id)
            {
                $category['id_category'] = $id;
                $resultCategory = $this->db->insert('game_category', $category);
            }
            
            //створюємо папку для галереї
            $dir = ROOT. 'img/gallery/' . $category['id_game'];
       
            $resultInsertGallery = $this->loadGallery($dir, $category['id_game'], $gallery);
            // якщо виникає помилка на одному з етапів видаляємо усе 
            if ($resultInsertGallery != true && $resultCategory != true) 
            {
                    $img = $values['image_url'];
                    $values[] = $category['id_game'];
                    $this->db->delete('game_category', $values);
                    unlink("$img");
                    $resultInsert = false;
            }
            
            
            if ($resultInsert == true)
            {
                echo "<script>alert(\"Гру додано\");</script>";
            }
            else
            {
                echo "<script>alert(\"Виникла помилка\");</script>";
            }    
        }
        else
        {
            echo "<script>alert(\"Виникла помилка при завантаженні зображення\");</script>";
        }    

    }


    public function deleteGame($values = [],$img)
    {
        $result = $this->db->delete('game', $values);

        $delImg = unlink("$img");

        $dir = ROOT. 'img/gallery/' . $values['id'];
        $resultGallery = $this->delDir($dir);

        $resultCategory = $this->db->delete('game_category', $values);

        if ($result == true && $delImg == true && $resultGallery == true)
        {
            echo "<script>alert(\"Гру видалено\");</script>";
        } 
        else
        {
            echo "<script>alert(\"Виникла помилка\");</script>";
        }     
    } 
    

    public function editGame($id)
    {
        if ($_FILES['image']['error'] == 0)
        {
            $img = $_POST['image_url'];
            $delImg = unlink("$img");
            
            if ($delImg == true)
            {
                $file = new File($_FILES['image']);
                $file->dir = 'img/game';
                $resultImg = $file->uploadImg();

                if ($resultImg == true)
                {
                    $_POST['image_url'] = 'img/game/' . $file->getName();
        
                }
                else
                {
                    echo "<script>alert(\"Виникла помилка\");</script>";
                    return false;
                }
            }

        }

        $idCategory = $_POST['id_category'];
        $category['id_game'] =  $id['id'];
        
        $this->db->delete('game_category', $category);

        foreach ($idCategory as $idVal)
        {
            $category['id_category'] = $idVal;
            $resultCategory = $this->db->insert('game_category', $category);
            if ($resultCategory != true) 
            {
                echo "<script>alert(\"Виникла помилка\");</script>";
                return false;
            }
        }

        unset($_POST['id_category']);

        if ($_FILES['img_slider']['error'][0] == 0)
        {
            $dir = ROOT. 'img/gallery/' . $id['id'];
            $resultGallery = $this->delDir($dir);
            $idGame['id_game'] = $id['id'];
            $resultDb = $this->db->delete('gallery', $idGame);

            if($resultGallery == true && $resultDb = true)
            {
                $resultInsertGallery = $this->loadGallery($dir, $id['id'], $_FILES['img_slider']);
                if ($resultInsertGallery != true) 
                {
                    echo "<script>alert(\"Виникла помилка\");</script>";
                    return false;
                }
            }
            else
            {
                echo "<script>alert(\"Виникла помилка\");</script>";
                return false;
            }
        }
        
        $resultUpGame = $this->db->update('game', $_POST, $id);
        if ($resultUpGame != true) 
        {
            echo "<script>alert(\"Виникла помилка\");</script>";
            return false;
        }
        else
        {
           echo "<script>alert(\"Гру редаговано\");</script>"; 
        }

        return true;
    }


    //видалення каталогу
    private function delDir($dir)
    {
        $open = opendir($dir);
        while (($file = readdir($open)) != false) {

                $pas = $dir . '/' . $file;

                if ($file != '..' && $file  != '.') 
                {
                   unlink($pas);
                }
                
            }
            
        return rmdir($dir);
            
    }

    private function loadGallery($dir, $id, $gallery)
    {
        mkdir($dir,0777);

        //формуємо масив зображень галереї
        $imgGallery = [];
        foreach ($gallery as $key => $value) 
        {
            $n = 0;
            foreach ($value as $imgVal) 
            {
                $nam = 'image' . $n;
                $imgGallery[$nam][$key] = $imgVal;
                $n++;
            }
        }

            //завантажуємо зображення галереї та записуємо в БД
        foreach ($imgGallery as $image)
        {
            $file = new File($image);
            $file->dir = $dir;
            $resultImgGallery = $file->uploadImg();

            $valGallery['image_url'] = '/img/gallery/' . $id . '/' . $file->getName();
            $valGallery['id_game'] = $id;
            $resultInsertGallery = $this->db->insert('gallery', $valGallery);
            // якщо виникає помилка на одному з етапів видаляємо усе 
            if ($resultInsertGallery != true && $resultImgGallery != true) 
            {
                    
                $values[] = $id;
                $this->db->delete('gallery', $values);
                $this->delDir($dir);
                    
                return false;
            }

        } 

        return true;  
    }

    public function getCount($table)
    {
        $sql = "SELECT COUNT(*) FROM `$table`";
        $result = $this->db->query($sql);
        $count = $result->fetch_assoc();
        return $count['COUNT(*)'];


    }

	private function __clone() {}
    private function __construct() {}



}
<?php

namespace system\session;

class Session
{

	protected $id;

	private static $instance = null;

    
	/**
     * getInstance create or return object
     * @return object | this object
     */
    public static function getInstance()
    {
        if (self::$instance === null) {

            self::$instance = new self();

            session_start();

            self::$instance->id = 'status';
        }
        
        return self::$instance;
    }

	public function setSession($status)
	{

		$_SESSION[$this->id] = $status;
	}
	

	public function getSession()
	{
		if (isset($_SESSION[$this->id])) {

			return $_SESSION[$this->id];

		}else {

			return false;
		}
		
	}


	public function clear()
	{

		session_unset();
	}


	private function __clone() {}
    private function __construct() {}
}
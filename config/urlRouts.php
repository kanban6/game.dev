<?php

/**
 * array with url route
 */
return [

    'urls' => [
        ''        => [
            'controller' => 'MVC\Controller\Pages',
            'action'     => 'home'
        ],
        'games' => [
            'controller' => 'MVC\Controller\Pages',
            'action'     => 'games'
        ],
        'contact' => [
            'controller' => 'MVC\Controller\Pages',
            'action'     => 'contact'
        ],
        'admin' => [
            'controller' => 'MVC\Controller\Admin',
            'action'     => 'login'
        ],
        'admin/home' => [
            'controller' => 'MVC\Controller\Admin',
            'action'     => 'home'
        ],
        'admin/logout' => [
            'controller' => 'MVC\Controller\Admin',
            'action'     => 'logout'
        ],
        'admin/add-game' => [
            'controller' => 'MVC\Controller\Admin',
            'action'     => 'addGame'
        ],
        'admin/add-slider' => [
            'controller' => 'MVC\Controller\Admin',
            'action'     => 'addSlider'
        ],
        'admin/add-category' => [
            'controller' => 'MVC\Controller\Admin',
            'action'     => 'addCategory'
        ],
        'admin/category' => [
            'controller' => 'MVC\Controller\Admin',
            'action'     => 'category'
        ],
        'admin/slider' => [
            'controller' => 'MVC\Controller\Admin',
            'action'     => 'slider'
        ],
        'admin/game' => [
            'controller' => 'MVC\Controller\Admin',
            'action'     => 'game'
        ],
        'admin/setting' => [
            'controller' => 'MVC\Controller\Admin',
            'action'     => 'setting'
        ]


   
    ],

    'pattern' => [

        'game\/[0-9]+' => [
            'controller' => 'MVC\Controller\Pages',
            'action'     => 'viewGame'
        ],
        'category\/[0-9]+' => [
            'controller' => 'MVC\Controller\Pages',
            'action'     => 'category'
        ],
        'search?+' => [
            'controller' => 'MVC\Controller\Pages',
            'action'     => 'search'
        ],
        'admin\/edit-category\/[0-9]+' => [
            'controller' => 'MVC\Controller\Admin',
            'action'     => 'editCategory'
        ],
        'admin\/edit-slider\/[0-9]+' => [
            'controller' => 'MVC\Controller\Admin',
            'action'     => 'editSlider'
        ],
        'admin\/edit-game\/[0-9]+' => [
            'controller' => 'MVC\Controller\Admin',
            'action'     => 'editGame'
        ]
 
    ]
  
];
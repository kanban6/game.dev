<?php

namespace MVC\Model;

use system\database\Database;

/**
* 
*/
class Pages
{
	
	private $db;

	function __construct()
	{
		$this->db = Database::getInstance();
	}

	public function getObjectDb($table, $id = false)
	{
		$sql = "SELECT * FROM $table";

		if($id != false)
		{
			$sql = "SELECT * FROM $table WHERE id='$id'";
		}

		$result = $this->db->getRow($sql);

		if ($result == true)
		{
			return $result;
		}

		return false;
	}

	public function getListCategory($id = false)
	{
		$sql = "SELECT category.*, game_category.id_game  FROM game JOIN game_category ON game.id = game_category.id_game JOIN category ON game_category.id_category = category.id";

		if($id)
		{
			$sql = "SELECT category.* FROM game JOIN game_category ON game.id = game_category.id_game JOIN category ON game_category.id_category = category.id WHERE game.id = '$id'";
		}
		
		
		$result = $this->db->getRow($sql);

		if ($result == true)
		{
			return $result;
		}

		return false;	
	}

	public function getHomeGame()
	{

		$sql = "SELECT id, title, image_url FROM game LIMIT 6";
		$result = $this->db->getRow($sql);

		if ($result == true)
		{
			return $result;
		}

		return false;	

	}

	public function getGallery($id)
	{
		$sql = "SELECT * FROM gallery WHERE id_game='$id'";
		
		$result = $this->db->getRow($sql);


		if ($result == true)
		{
			return $result;
		}

		return false;
	}

	public function getCategory($id = false)
	{
		$sql = "SELECT * FROM category ORDER BY number";

		if ($id)
		{
			$sql = "SELECT * FROM category 	WHERE id='$id'";
		}
		
		$result = $this->db->getRow($sql);

		if ($result == true)
		{
			return $result;
		}

		return false;
	}

	public function getGameCategory($id)
	{
		$sql = "SELECT game.* FROM game JOIN game_category ON game.id = game_category.id_game JOIN category ON game_category.id_category = category.id WHERE game_category.id_category = '$id'";

		
		$result = $this->db->getRow($sql);

		if ($result == true)
		{
			return $result;
		}

		return false;
	}

	public function getGameSearch()
	{
		$value = htmlspecialchars($_GET['search']);

		$sql = "SELECT * FROM game WHERE description LIKE '%$value%'";

		$result = $this->db->getRow($sql);

		if ($result == true)
		{
			return $result;
		}

		return false;
	}


}
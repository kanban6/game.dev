<?php

/**
 * array tables databese table=>field=>type
 */
return [

	'category' => [

			'id'      => 'int(6) AUTO_INCREMENT KEY',
			'number'  => 'int(6) NOT NULL',
			'title'   => 'varchar(50) NOT NULL'
	],	

	'admin'   => [

			'id'       => 'int(1) AUTO_INCREMENT KEY NOT NULL',
			'login'    => 'varchar(10) NOT NULL',
			'password' => 'varchar(60) NOT NULL',
			'email'    => 'varchar(20) NOT NULL',

	],
	'slider'   => [

			'id'          => 'int(3) AUTO_INCREMENT KEY',
			'number'      => 'int(6) NOT NULL',
			'title'       => 'varchar(100) NOT NULL',
			'description' => 'varchar(250) NOT NULL',
			'image_url'   => 'varchar(100) NOT NULL',
			'link'        => 'varchar(250) NOT NULL'
	],
	'gallery'   => [

			'id'        => 'int(6) AUTO_INCREMENT KEY NOT NULL',
			'image_url' => 'varchar(100) NOT NULL',
			'id_game'   => 'int(6) NOT NULL'
	],
	'game'   => [

			'id'          => 'int(6) AUTO_INCREMENT KEY NOT NULL',
			'image_url'   => 'varchar(100) NOT NULL',
			'title'       => 'varchar(100) NOT NULL',
			'id_category' => 'int(6) NOT NULL',
			'description' => 'TEXT',
			'OS'          => 'varchar(50) NOT NULL',
			'prots'       => 'varchar(50) NOT NULL',
			'RAM'         => 'int(4) NOT NULL',
			'video'       => 'varchar(50) NOT NULL',
			'memory'      => 'varchar(50) NOT NULL',
			'status'      => 'int(1) NOT NULL',
			'link'        => 'varchar(250) NOT NULL'
	],
	'game_category'   => [
			'id'          => 'int(6) AUTO_INCREMENT KEY NOT NULL',
			'id_game'   => 'int(6) NOT NULL',
			'id_category'   => 'int(6) NOT NULL'

	],




];
<?php

namespace MVC\Controller;

use system\Controller;
use system\Router;
use MVC\Model\Admin;
use MVC\Model\Pages;

/**
* 
*/
class AdminController extends Controller
{
	
	/**
	 * [logStatus description]
	 * @return [type] [description]
	 */
	private function logStatus()
	{
		
		$admin = Admin::getInstance();
		
		$status = $admin->logStatus();
		
		if ($status == false) {

			header("Location: /admin");	
			die('You are not logged in');
		}
		
	}


	/**
	 * [loginAction description]
	 * @return [type] [description]
	 */
	public function loginAction()
	{

		$admin = Admin::getInstance();

		if (isset($_POST['login']) && isset($_POST['password'])) {
		
			$result = $admin->logIn($_POST['login'], $_POST['password']);

			if ($result) {

				header("Location: /admin/home");	
				
			}else {

				$error = 'Логін або пароль введено невірно';
				$this->setContent('error', $error);
			}

		}

		$title = 'Вхід - Адмін панель';
		
		$this->setContent('title', $title);
		$this->View('Admin/login', 'default');

	}


	/**
	 * [homeAction description]
	 * @return [type] [description]
	 */
	public function homeAction()
	{
		$this->logStatus();
		$title = 'Головна - Адмін панель';
		
		$this->setContent('title', $title);
		
		$admin = Admin::getInstance();
		$gameCount = $admin->getCount('game');
		$this->setContent('gameCount', $gameCount);

		$categoryCount = $admin->getCount('category');
		$this->setContent('categoryCount', $categoryCount);

		$sliderCount = $admin->getCount('slider');
		$this->setContent('sliderCount', $sliderCount);

		$page = new Pages;
		$adminInfo = $page->getObjectDb('admin');
		$adminInfo = $adminInfo[0];
		$this->setContent('adminInfo', $adminInfo);

		$this->View('Admin/home', 'admin');

	}


	/**
	 * [logoutAction description]
	 * @return [type] [description]
	 */
	public function logoutAction()
	{
		$admin = Admin::getInstance();

		$admin->logOut();
		header("Location: /admin");	
	}

	public function addGameAction()
	{

		$this->logStatus();
		$title = 'Додати гру - Адмін панель';
		$this->setContent('title', $title);

		$page = new Pages;
		$category = $page->getObjectDb('category');
		$this->setContent('category', $category);

		$this->View('Admin/addGame', 'admin');

		if (isset($_POST['addGame'])) 
		{

			unset($_POST['addGame']);
			$admin = Admin::getInstance();
			$admin->addGame($_FILES['image'], $_FILES['img_slider'], $_POST);
		}
	}

	public function gameAction()
	{
		$this->logStatus();
		$title = 'Редагувати гру - Адмін панель';
		$this->setContent('title', $title);

		if (isset($_POST['deleteGame'])) 
		{
			$image = $_POST['image'];
			unset($_POST['deleteGame']);
			unset($_POST['image']);
			$admin = Admin::getInstance();
			$admin->deleteGame($_POST, $image);
		}	

		$page = new Pages;
		$game = $page->getObjectDb('game'); 
		$this->setContent('game', $game);

		$this->View('Admin/game', 'admin');
	}

	public function editGameAction()
	{
		$this->logStatus();

		$url = trim($_SERVER['REQUEST_URI'], '/');
		$urls = explode('/', $url);
		$id['id'] = $urls[2];
		
		if (isset($_POST['editGame'])) 
		{

			unset($_POST['editGame']);
			$admin = Admin::getInstance();
			$admin->editGame($id);
		}

		$page = new Pages;
		$category = $page->getObjectDb('category');
		$this->setContent('category', $category);

		$game = $page->getObjectDb('game', $id['id']); 
		$game = $game[0];
		$this->setContent('game', $game);

		$categoryList = $page->getListCategory($id['id']); 

		$this->setContent('categoryList', $categoryList);

		$title = 'Редагувати гру ' . $game->title . '- Адмін панель' ;
		$this->setContent('title', $title);

		$gallery = $page->getGallery($id['id']);
		$this->setContent('gallery', $gallery);

		$this->View('Admin/editGame', 'admin');
		
	}

	public function addSliderAction()
	{
		$this->logStatus();
		$title = 'Додати слайдер - Адмін панель';
		$this->setContent('title', $title);
		$this->View('Admin/addSlider', 'admin');
		

		if (isset($_POST['addSlider'])) 
		{

			unset($_POST['addSlider']);
			$admin = Admin::getInstance();
			$admin->addSlider($_FILES['slider'], $_POST);
		}
	
	}

	public function addCategoryAction()
	{
		$this->logStatus();
		$title = 'Додати категорію - Адмін панель';
		$this->setContent('title', $title);
		$this->View('Admin/addCategory', 'admin');

		if (isset($_POST['addCategory'])) 
		{

			unset($_POST['addCategory']);
			$admin = Admin::getInstance();
			$admin->addCategory($_POST);
		}	
	}

	public function categoryAction()
	{
		$this->logStatus();
		$title = 'Редагувати категорію - Адмін панель';
		$this->setContent('title', $title);

		if (isset($_POST['deleteCategory'])) 
		{

			unset($_POST['deleteCategory']);
			$admin = Admin::getInstance();
			$admin->deleteCategory($_POST);
		}	
		
		$page = new Pages;
		$category = $page->getObjectDb('category'); 
		$this->setContent('category', $category);

		$this->View('Admin/category', 'admin');

		
	}

	public function editCategoryAction()
	{
		$this->logStatus();
		$title = 'Редагувати категорію - Адмін панель';
		$this->setContent('title', $title);

		$url = trim($_SERVER['REQUEST_URI'], '/');
		$urls = explode('/', $url);
		$id['id'] = $urls[2];

		if (isset($_POST['editCategory'])) 
		{

			unset($_POST['editCategory']);
			$admin = Admin::getInstance();
			$admin->editCategory($_POST, $id);
		}	

		$page = new Pages;
		$category = $page->getObjectDb('category' ,$id['id']); 
		$category = $category[0];
		$this->setContent('category', $category);

		$this->View('Admin/editCategory', 'admin');
	}

	public function sliderAction()
	{
		$this->logStatus();
		$title = 'Редагувати слайдер - Адмін панель';
		$this->setContent('title', $title);

		if (isset($_POST['deleteSlider'])) 
		{
			$image = $_POST['image'];
			unset($_POST['deleteSlider']);
			unset($_POST['image']);
			$admin = Admin::getInstance();
			$admin->deleteSlider($_POST, $image);
		}	

		$page = new Pages;
		$slider = $page->getObjectDb('slider'); 
		$this->setContent('slider', $slider);

		$this->View('Admin/slider', 'admin');
	}

	public function editSliderAction()
	{
		$this->logStatus();
		$title = 'Редагувати слайдер - Адмін панель';
		$this->setContent('title', $title);

		$url = trim($_SERVER['REQUEST_URI'], '/');
		$urls = explode('/', $url);
		$id['id'] = $urls[2];

		if (isset($_POST['editSlider'])) 
		{

			unset($_POST['editSlider']);
			$admin = Admin::getInstance();
			$admin->editSlider($id);
		}	

		$page = new Pages;
		$slider = $page->getObjectDb('slider', $id['id']); 
		$slider = $slider[0];

		$this->setContent('slider', $slider);

		$this->View('Admin/editSlider', 'admin');
	}

	public function settingAction()
	{
		$this->logStatus();
		$title = 'Налаштування';
		$this->setContent('title', $title);

		if(isset($_POST['editPassword']))
		{
			$admin = Admin::getInstance();
			$admin->editPassword($_POST['password'], $_POST['newPassword'], $_POST['rePassword']);
		}

		if(isset($_POST['editEmail']))
		{
			$admin = Admin::getInstance();
			$admin->editEmail($_POST['password'], $_POST['newEmail']);
		}


		$this->View('Admin/setting', 'admin');
	}

}
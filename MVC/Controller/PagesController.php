<?php

namespace MVC\Controller;

use system\Controller;
use system\Mail;
use MVC\Model\Pages;

class PagesController extends Controller
{
	
	public function homeAction()
	{
		$title = 'Головна';
		$this->setContent('title', $title);

		$page = new Pages;
		$slider = $page->getObjectDb('slider'); 

		$games = $page->getHomeGame();

		$this->setContent('games', $games);
		$this->setContent('slider', $slider);

		$this->View('home');
	}

	public function gamesAction()
	{
		$title = 'Ігри';
		$this->setContent('title', $title);

		$page = new Pages;
		$category = $page->getCategory(); 
		$this->setContent('category', $category);

		$games = $page->getObjectDb('game');
		$this->setContent('games', $games);
		$gameCategory = $page->getListCategory();
		$this->setContent('gameCategory', $gameCategory);

		$this->View('games');
	}

	public function viewGameAction ()
	{
		$title = 'Перегляд гри';
		$this->setContent('title', $title);

		$page = new Pages;
		$category = $page->getCategory(); 
		$this->setContent('category', $category);

		$url = trim($_SERVER['REQUEST_URI'], '/');
		$urls = explode('/', $url);
		$id['id'] = $urls[1];

		$page = new Pages;
		$gallery = $page->getGallery($id['id']); 

		$game = $page->getObjectDb('game', $id['id']); 
		$game = $game[0];
		if ($game == NULL)
		{
			$this->notFound();
		}
		$this->setContent('game', $game);

		$this->setContent('gallery', $gallery);

		$this->View('viewGame');
	}

	public function categoryAction()
	{

		$url = trim($_SERVER['REQUEST_URI'], '/');
		$urls = explode('/', $url);
		$id = $urls[1];

		$page = new Pages;
		$category = $page->getCategory(); 
		$this->setContent('category', $category);

		foreach ($category as  $value) {
			if ($id == $value->id) {
				$title = 'Категорія ' . $value->title;
			}
		}
		
		$this->setContent('title', $title);

		$games = $page->getGameCategory($id);
		if ($games == NULL)
		{
			$this->notFound();
		}
		$this->setContent('games', $games);
		$gameCategory = $page->getListCategory();
		$this->setContent('gameCategory', $gameCategory);

		$this->View('games');

	}

	public function contactAction ()
	{
		$title = 'Контакти';
		$this->setContent('title', $title);

		$page = new Pages;
		$category = $page->getCategory(); 
		$this->setContent('category', $category);

		$this->View('contact');
		if(isset($_POST['send']))
		{
			$mail = new Mail($_POST['name'], $_POST['email'], $_POST['message']);	
			$mail->send();
			
		}	
		
	}


	public function searchAction()
	{
		$title = 'Результат пошуку ' . $_GET['search'];
		$this->setContent('title', $title);

		$page = new Pages;
		$category = $page->getCategory(); 
		$this->setContent('category', $category);
		
		$games = $page->getGameSearch();
		$this->setContent('games', $games);
		$gameCategory = $page->getListCategory();
		$this->setContent('gameCategory', $gameCategory);

		$this->View('search');
		
	}

	public function notFound()
	{
		$title = 'Сторінка не знайдена';
		$this->setContent('title', $title);
		Controller::View('404');
	}

}